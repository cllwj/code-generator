package com.dwp.codegenerator.utils;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(
        value = "接口返回对象",
        description = "接口返回对象"
)
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("返回处理消息")
    private String message = "操作成功！";
    @ApiModelProperty("返回代码")
    private Integer status = 200;
    @ApiModelProperty("返回数据对象")
    private T data;
    @ApiModelProperty("时间戳")
    private long timestamp = System.currentTimeMillis();

    public static synchronized <T> ResponseResult<T> e(ResponseCode statusEnum) {
        return e(statusEnum, null);
    }

    public static synchronized <T> ResponseResult<T> ok(String message) {
        ResponseResult<T> res = new ResponseResult();
        res.setStatus(ResponseCode.OK.code);
        res.setMessage(message);
        res.setData(null);
        return res;
    }

    public static synchronized <T> ResponseResult<T> error(String message) {
        ResponseResult<T> res = new ResponseResult();
        res.setStatus(ResponseCode.FAIL.code);
        res.setMessage(message);
        res.setData(null);
        return res;
    }

    public static synchronized <T> ResponseResult<T> e(ResponseCode statusEnum, T data) {
        ResponseResult<T> res = new ResponseResult();
        res.setStatus(ResponseCode.OK.code);
        res.setMessage(statusEnum.msg);
        res.setData(data);
        return res;
    }

    public static <T> ResponseResultBuilder<T> builder() {
        return new ResponseResultBuilder();
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getStatus() {
        return this.status;
    }

    public T getData() {
        return this.data;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setStatus(final Integer status) {
        this.status = status;
    }

    public void setData(final T data) {
        this.data = data;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ResponseResult;
    }

    @Override
    public String toString() {
        return "ResponseResult(message=" + this.getMessage() + ", status=" + this.getStatus() + ", data=" + this.getData() + ", timestamp=" + this.getTimestamp() + ")";
    }

    public ResponseResult() {
    }

    public ResponseResult(final String message, final Integer status, final T data, final long timestamp) {
        this.message = message;
        this.status = status;
        this.data = data;
        this.timestamp = timestamp;
    }

    public static class ResponseResultBuilder<T> {
        private String message;
        private Integer status;
        private T data;
        private long timestamp;

        ResponseResultBuilder() {
        }

        public ResponseResultBuilder<T> message(final String message) {
            this.message = message;
            return this;
        }

        public ResponseResultBuilder<T> status(final Integer status) {
            this.status = status;
            return this;
        }

        public ResponseResultBuilder<T> data(final T data) {
            this.data = data;
            return this;
        }

        public ResponseResultBuilder<T> timestamp(final long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public ResponseResult<T> build() {
            return new ResponseResult(this.message, this.status, this.data, this.timestamp);
        }

        @Override
        public String toString() {
            return "ResponseResult.ResponseResultBuilder(message=" + this.message + ", status=" + this.status + ", data=" + this.data + ", timestamp=" + this.timestamp + ")";
        }
    }
}

