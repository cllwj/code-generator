package com.dwp.codegenerator.utils;

import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ResponseMessage;

import java.util.ArrayList;
import java.util.List;

public enum ResponseCode {

    OK(200, "操作成功"),
    FAIL(500, "操作失败"),
    NO_AUTH(403, "没有权限"),
    NO_PAGE(404, "未找到页面"),
    TOKEN_OVERDUE(400, "Token超期"),
    NO_TASKMANAGER(804, "任务管理服务器不在线"),
    NO_BIGDATA_SERVER(805, "大数据库服务器不在线"),
    RECEIVE_PART_REQUEST(806, "部分请求已收到"),
    NOTREADY_BIGDATA_SERVICE(807, "大数据库服务没有准备好");

    public Integer code;
    public String msg;

    public static List<ResponseMessage> getArrayMessage() {
        ArrayList<ResponseMessage> responseMessages = new ArrayList();
        ResponseCode[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            ResponseCode statusEnum = var1[var3];
            responseMessages.add((new ResponseMessageBuilder()).code(statusEnum.code).message(statusEnum.msg).build());
        }

        return responseMessages;
    }

    private ResponseCode() {
    }

    private ResponseCode(final Integer code, final String msg) {
        this.code = code;
        this.msg = msg;
    }
}
